<table class="table table-condensed table-striped table-bordered">
	<thead>
		<tr>
			<th>Numéro de genre</th>
			<th>Nom de la catégorie</th>
		
		</tr>
	</thead>
	<tbody>
		<?php foreach ($genres as $genre): ?>
			<tr>
				<td><?php echo "<a href='index.php?action=classification/fiche_genre&id=".$genre['code_genre']."'>".$genre['code_genre']."</a>"; ?></td>
				<td><?php echo $genre['nom_genre'] ?></td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>