
<?php 

$file= fopen("views/classification/Films_d_un_genre.xml", "w+");
$_xml ="<?xml version=\"1.0\" encoding=\"iso-8859-1\"?> \r\n";

 
 echo "<a href='views/classification/Films_d_un_genre.xml'>Voir le xml</a>";

	foreach($genre as $gen):{
		echo "<p>Tous les films appartenants au genre : ".$gen['nom_genre']."</p>";
	}
	endforeach;
$_xml .="<body>\r<titre>Tous les films appartenants au genre : ".$gen['nom_genre']."</titre>\r\n";
$_xml .= "<films>\r";
	
	echo "
	<table class='table table-condensed table-striped table-bordered'>
	<thead>
		<tr>
			<th>Code du film</th>
			<th>titre original</th>
			<th>titre en français</th>
			<th>Date</th>
			<th>durée</th>
		</tr>
	</thead>
	<tbody>";

	foreach($films as $film):
	{
		echo "
			<tr>
				<td><a href='index.php?action=films/fiche_film&id=".$film['code_film']."'>".$film['code_film']."</a></td>
				<td>".$film['titre_original']."</td>
				<td>".$film['titre_francais']."</td>
				<td>".$film['date']."</td>
				<td>".$film['duree']." min</td>
			</tr>";
			
		$_xml .= "  <film TestId=\"".$film['code_film']."\">
					<titre_original>".str_replace('&', '&amp;', $film['titre_original'])."</titre_original>
					<titre_francais>".str_replace('&', '&amp;', $film['titre_francais'])."</titre_francais>
					<date>".$film['date']."</date>
					<duree>".$film['duree']."</duree>
				</film>\r\n";
	}
	endforeach;
	
$_xml .= "</films>\r</body>";
echo "</tbody></table>";
fwrite($file, $_xml);
fclose($file);

?>