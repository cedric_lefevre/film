<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title><?php echo $page_title ?> - LP / bugtracker</title>
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" type="text/css" media="screen" title="no title" charset="utf-8">
		<script src="http://code.jquery.com/jquery.js"></script>
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
	</head>
	<body class="container">
		<div class="row">
			<div class="col-md-2 well">
				<ul class="nav nav-pills nav-stacked">
					<li><a href="?action=home">Accueil</a></li>
					<li><a href="?action=acteurs/list_act">liste des acteurs</a></li>
					<li><a href="?action=acteurs/list_rea">liste des réalisateurs</a></li>
					<li><a href="?action=films/list">liste des films</a></li>
					<li><a href="?action=classification/list_genres">liste des genres</a></li>
					<li><a href="?action=search">Page de recherche</a></li>
				</ul>
			</div>
			<div class="col-md-10">
				<div class="page-header">
					<h1><?php echo $page_title ?> <small> - lpbt manager</small></h1>
				</div>
				<?php echo $content?>
			</div>
		</div>
	</body>
</html>
