<table class="table table-condensed table-striped table-bordered">
	<thead>
		<tr>
			<th>Code de l'individu</th>
			<th>nom</th>
			<th>prénom</th>
			<th>Nationalité</th>		
			<th>Date de naissance</th>
			<th>Date éventuelle de mort</th>
			</tr>
	</thead>
	<tbody>
		<?php foreach ($indivs as $indiv): ?>
			<tr>
				<td><?php echo "<a href='index.php?action=acteurs/fiche_individus&id=".$indiv['code_indiv']."'>".$indiv['code_indiv']."</a>"; ?></td>
				<td><?php echo $indiv['nom'] ?></td>
				<td><?php echo $indiv['prenom'] ?></td>
				<td><?php echo $indiv['nationalite'] ?></td>
				<td><?php echo $indiv['date_naiss'] ?></td>
				<td><?php echo $indiv['date_mort'] ?></td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>