<table class="table table-condensed table-striped table-bordered">
	<thead>
		<tr>
			<th>Code de l'individu</th>
			<th>nom</th>
			<th>prénom</th>
			<th>Nationalité</th>		
			<th>Date de naissance</th>
			<th>Date éventuelle de mort</th>
			</tr>
	</thead>
	<tbody>
		<?php foreach ($reas as $rea): ?>
			<tr>
				<td><?php echo "<a href='index.php?action=acteurs/fiche_individus&id=".$rea['code_indiv']."'>".$rea['code_indiv']."</a>"; ?></td>
				<td><?php echo $rea['nom'] ?></td>
				<td><?php echo $rea['prenom'] ?></td>
				<td><?php echo $rea['nationalite'] ?></td>
				<td><?php echo $rea['date_naiss'] ?></td>
				<td><?php echo $rea['date_mort'] ?></td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>


