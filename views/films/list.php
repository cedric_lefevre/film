<table class="table table-condensed table-striped table-bordered">
	<thead>
		<tr>
			<th>Code du film</th>
			<th>titre original</th>
			<th>titre en français</th>
			<th>pays</th>
			<th>Date</th>
			<th>durée</th>
			<th>couleur</th>
			<th>réalisateur</th>
		
		</tr>
	</thead>
	<tbody>
		<?php foreach ($films as $film): ?>
			<tr>
				<td><?php echo "<a href='index.php?action=films/fiche_film&id=".$film['code_film']."'>".$film['code_film']."</a>"; ?></td>
				<td><?php echo $film['titre_original'] ?></td>
				<td><?php echo $film['titre_francais'] ?></td>
				<td><?php echo $film['pays'] ?></td>
				<td><?php echo $film['date'] ?></td>
				<td><?php echo $film['duree'] ?></td>
				<td><?php echo $film['couleur'] ?></td>
				<td><?php echo "<a href='index.php?action=acteurs/fiche_individus&id=".$film['realisateur']."'>".$film['prenom']." ".$film['nom']."</a>"; ?></td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>