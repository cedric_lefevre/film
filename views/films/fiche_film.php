
<?php 
	foreach($films as $film):{
		echo "<p>".$film['titre_francais']."( ".$film['titre_original']." )</p>";
		echo "<p>Durée :".$film['duree']." min</p>";
		echo "<p>Date :".$film['date']."</p>";
		echo "<p>Pays :".$film['pays']."</p>";
	}
	endforeach;
	
	foreach($realisateur as $real):{
		echo "<p>Réalisateur : <a href='index.php?action=acteurs/fiche_individus&id=".$real['realisateur']."'>".$real['prenom'].' '.$real['nom']."</a></p>";
	}
	endforeach;
	
	echo "Genres : <ul>";
	
	foreach($genres as $genre):{
		echo '<li><a href=\'index.php?action=classification/fiche_genre&id='.$genre['code_genre'].'\'>'.$genre['nom_genre'].'</a></li>';
	}
	endforeach;

	
	echo "</ul><p>Acteurs ayant participés à ce film :</p>
	<table class='table table-condensed table-striped table-bordered'>
	<thead>
		<tr>
			<th>Id</th>
			<th>prenom</th>
			<th>nom</th>
			<th>date de naissance</th>
			<th>date de mort</th>
			<th>nationnalité</th>
		</tr>
	</thead>
	<tbody>";
	
	foreach($acteurs as $acteur):
	{
		echo "
			<tr>
				<td><a href='index.php?action=acteurs/fiche_individus&id=".$acteur['code_indiv']."'>".$acteur['code_indiv']."</a></td>
				<td>".$acteur['prenom']."</td>
				<td>".$acteur['nom']."</td>
				<td>".$acteur['date_naiss']."</td>
				<td>".$acteur['date_mort']."</td>
				<td>".$acteur['nationalite']."</td>
			</tr>";
	}
	endforeach;
	
	
	echo "</tbody></table>";

?>