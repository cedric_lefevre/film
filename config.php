<?php

/**
 * fonction pour afficher les erreurs
 */ 
function debug($datas,$die=false)
{
	echo '<pre>';
	var_export($datas);
	echo '</pre>';
	if($die) die();
}

/**
 * défini les chemin qui vont nous servir tout au long de laplication
 * pour accéder au models, vues et controllers
 */
define('PATH_CONTROLERS',dirname(__FILE__).'/controllers');
define('PATH_MODELS',dirname(__FILE__).'/models');
define('PATH_VIEWS',dirname(__FILE__).'/views');
?>
