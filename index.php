<?php
require_once 'config.php';

// Quelle action ? Exécution de l'action demandé
$action = empty($_REQUEST['action']) ? 'home' : $_REQUEST['action'];
$id = empty($_REQUEST['id']) ? '' : $_REQUEST['id'];
include_once PATH_CONTROLERS.'/'.$action.'.php';

// Rendu du templating Action
ob_start();
include_once PATH_VIEWS.'/'.$action.'.php';
$content = ob_get_contents();
ob_end_clean();

// Rendu du templating Global
include_once PATH_VIEWS.'/layout.php';


?>
