<?php
require PATH_MODELS.'/films.php';
require PATH_MODELS.'/individus.php';

$id = (int)$_GET['id'];
$o = new Film();
$p = new Indiv();

$films = $o->get_film($id);	
$realisateur = $p->get_realisateur($id);
$acteurs = $o->get_acteurs_film($id);
$genres = $o->get_genres_film($id);

$page_title = 'Fiche de film';

?>