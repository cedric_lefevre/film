<?php
if(!$_POST){
//récuprération du modèle
	require PATH_MODELS.'\films.php';
	require PATH_MODELS.'\individus.php';

//création d'une entité de la classe
	$i=new Indiv;
	$f=new Film;

//on récupère le contue d'une fonction dans une variable PDO::STATEMENT
	$nats=$i->pays();
	$pays=$f->pays();

//Titre de la page
	$page_title='recherche';
}	

else {

 //Test le formulaire choisit
if($_GET['param']=='acteurs'){ 

//récuprération du modèle
	require PATH_MODELS.'\individus.php';
	
//création d'une entité de la classe
	$i=new Indiv;
	
//Titre de la page
	$page_title='individus recherchés';
	
//remplace les caractères spéciaux par des caractères simples pour être compris par la base de données
	$nat=str_replace('é', 'e', $_POST['nat']);
	$nat=str_replace('ç', 'c', $_POST['nat']);
	
// 3 de bon
	if($_POST['name'] and $_POST['surname'] and $nat!=1)
		$indivs=$i->get_act_all($_POST['name'] , $_POST['surname'] , $nat);
// 2 de bon
	else if ($_POST['name'] and $_POST['surname'] and $nat==1)
		$indivs=$i->get_act_name_surname($_POST['name'] , $_POST['surname'] );
	else if($_POST['name'] and !$_POST['surname'] and $nat!=1)
		$indivs=$i->get_act_name_nat($_POST['name'] , $nat);
	else if(!$_POST['name'] and $_POST['surname'] and $nat!=1)
		$indivs=$i->get_act_surname_nat( $_POST['surname'] , $nat);
// 1 de bon 
	else if($_POST['name'] and !$_POST['surname'] and $nat==1)
		$indivs=$i->get_act_name($_POST['name'] );
	else if(!$_POST['name'] and $_POST['surname'] and $nat==1)
		$indivs=$i->get_act_surname($_POST['surname']);
	else if(!$_POST['name'] and !$_POST['surname'] and $nat!=1){
		$indivs=$i->get_act_nat(str_replace('é', 'e', $nat));	
		
		}
 }

else if($_GET['param']=='films'){ 
//récuprération du modèle
	require PATH_MODELS.'\films.php';
	
//création d'une entité de la classe
	$f=new Film;
	
//Titre de la page
	$page_title='films recherchés';
	
// 4 de bons 
	if($_POST['tiO'] and $_POST['tiFr'] and $_POST['pays']!=1 and $_POST['duree']!=0)
			$films=$f->all($_POST['tiO'] , $_POST['tiFr'] , $_POST['pays'] , $_POST['duree']);
// 3 de bons 
	else if ($_POST['tiO'] and $_POST['tiFr'] and $_POST['pays']!=1 and $_POST['duree']==0)
			$films=$f->ori_fr_pay($_POST['tiO'] , $_POST['tiFr'] , $_POST['pays'] );
	else if($_POST['tiO'] and $_POST['tiFr'] and $_POST['pays']==1 and $_POST['duree']!=0)
			$films=$f->ori_fr_duree($_POST['tiO'] , $_POST['tiFr'] , $_POST['duree']);	
	else if($_POST['tiO'] and !$_POST['tiFr'] and $_POST['pays']!=1 and $_POST['duree']!=0)
			$films=$f->ori_pay_duree($_POST['tiO'] , $_POST['pays'] , $_POST['duree']);
	else	if(!$_POST['tiO'] and $_POST['tiFr'] and $_POST['pays']!=1 and $_POST['duree']!=0)
			$films=$f->fr_pay_duree($_POST['tiFr'] , $_POST['pays'] , $_POST['duree']);
// 2 de bon avec ori
	else	if($_POST['tiO'] and $_POST['tiFr'] and $_POST['pays']==1 and $_POST['duree']==0)
			$films=$f->ori_fr($_POST['tiO'] , $_POST['tiFr'] );
	else	if($_POST['tiO'] and !$_POST['tiFr'] and $_POST['pays']!=1 and $_POST['duree']==0)
			$films=$f->ori_pay($_POST['tiO'] , $_POST['pays']);
	else	if($_POST['tiO'] and !$_POST['tiFr'] and $_POST['pays']==1 and $_POST['duree']!=0)
			$films=$f->ori_duree($_POST['tiO'] ,$_POST['duree']);
// 2 de bon sans ori
	else	if(!$_POST['tiO'] and $_POST['tiFr'] and $_POST['pays']!=1 and $_POST['duree']==0)
			$films=$f->fr_pay( $_POST['tiFr'] , $_POST['pays'] );
	else	if(!$_POST['tiO'] and $_POST['tiFr'] and $_POST['pays']==1 and $_POST['duree']!=0)
			$films=$f->fr_duree( $_POST['tiFr'] ,  $_POST['duree']);
	else	if(!$_POST['tiO'] and !$_POST['tiFr'] and $_POST['pays']!=1 and $_POST['duree']!=0)
			$films=$f->pay_duree($_POST['pays'] , $_POST['duree']);
// 1 de bon
	else if($_POST['tiO'] and !$_POST['tiFr'] and $_POST['pays']==1 and $_POST['duree']==0)
		$films=$f->ori($_POST['tiO'] );
	else if(!$_POST['tiO'] and $_POST['tiFr'] and $_POST['pays']==1 and $_POST['duree']==0)
		$films=$f->fr($_POST['tiFr'] );	
	else if(!$_POST['tiO'] and !$_POST['tiFr'] and $_POST['pays']!=1 and $_POST['duree']==0)
		$films=$f->pay($_POST['pays'] );
	else
		$films=$f->duree($_POST['duree'] );
	} 
else $page_title='erreur';


}
?>