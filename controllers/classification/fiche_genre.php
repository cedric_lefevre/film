<?php
require PATH_MODELS.'/genres.php';

$id = (int)$_GET['id'];
$o = new Genre();

$genre = $o->get_genre($id);
$films = $o->get_films_genre($id);

$page_title = 'Fiche d\'un genre';

?>