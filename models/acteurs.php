<?php
require_once 'base.php';

/**
* 
*/
class Act extends base
{
	/**
	 * liste tout individus qui est acteur d'un film
	 */ 
		public function liste_acteurs()
	{
		$q = 'SELECT * FROM individus where code_indiv in( SELECT distinct ref_code_acteur from acteurs) ORDER BY code_indiv';
		return $this->getPdo()->query($q);
	}
	
	/**
	 * liste tout individus qui est réalisateur d'un film
	 */ 
	public function liste_rea()
	{
		$q = 'SELECT * FROM individus where code_indiv in( SELECT distinct realisateur from films) ORDER BY code_indiv';
		return $this->getPdo()->query($q);
	}


	/**
	 * liste tout film joué par l'individu ayant $id
	*/ 

	public function get_films_acteur($id){
		$q = '	SELECT 	distinct code_film, titre_original, titre_francais, date, duree   from 	individus, acteurs, films 
				where	individus.code_indiv=acteurs.ref_code_acteur
				AND 	acteurs.ref_code_film=films.code_film
				AND 	code_indiv = \''.$id.'\'';
		return $this->getPdo()->query($q);
	}
	
	/**
	 * liste tout film réalisé par l'individu ayant $id
	*/ 
	
	public function get_films_realisateur($id){
		$q = '	SELECT 	distinct code_film, titre_original, titre_francais, date, duree  from 	individus, acteurs, films 
				where	individus.code_indiv=acteurs.ref_code_acteur
				AND 	acteurs.ref_code_film=films.code_film
				AND 	realisateur = \''.$id.'\'';
		return $this->getPdo()->query($q);
	}

}


?>
