<?php
require_once 'base.php';

/**
* 
*/
class Film extends base
{
	/**
	 * liste de tous les films
	 */ 
	public function liste_simple()
	{
		$q = 'SELECT * FROM films, individus where films.realisateur=individus.code_indiv ORDER BY code_film';
		return $this->getPdo()->query($q);
	}
	
	
	 /**
	 * avoir les acteurs ayant joués ce film
	 */ 
	public function get_acteurs_film($id){
		$q = '	SELECT 	*   from 	individus, acteurs, films 
				where	individus.code_indiv=acteurs.ref_code_acteur
				AND 	acteurs.ref_code_film=films.code_film
				AND 	code_film = \''.$id.'\'';
		return $this->getPdo()->query($q);
	}
	
		/**
	 * les genres d'un film
	 */ 
	public function get_genres_film($id){
		$q = '	SELECT 	*   from 	classification, genres, films 
				where	classification.ref_code_genre=genres.code_genre
				AND 	classification.ref_code_film=films.code_film
				AND 	code_film = \''.$id.'\'
				order by code_film';
		return $this->getPdo()->query($q);
	}	
	
		/**
	 * le film ayant $id
	 */ 
	
	public function get_film($id)
	{
		$q = 'SELECT * FROM films, individus where films.realisateur=individus.code_indiv AND code_film='.$id;
		return $this->getPdo()->query($q);
	}

	/**
	 * avoir la liste des pays des films de la base
	 */ 
	public function pays(){
        $q = 'SELECT  distinct pays from films ;';
        return $this->getPdo()->query($q);
    }


	public function ori($or){
        $q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND titre_original=\''.$or.'\';';
        return $this->getPdo()->query($q);
    }

/*******    Fonctions pour la recherche de film 	*******/

    public function ori_fr($or, $fr){
        $q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND titre_original=\''.$or.'\'and titre_francais=\''.$fr.'\';';
        return $this->getPdo()->query($q);
    }

    public function ori_fr_pay($or, $fr, $pa){
        $q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND titre_original=\''.$or.'\' and titre_francais=\''.$fr.'\' and  pays=\''.$pa.'\';';
        return $this->getPdo()->query($q);
    }


    public function ori_fr_duree($ori, $fr, $ind){
    	if($ind==1) 
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree<=60 and titre_original=\''.$or.'\' and  titre_francais=\''.$fr.'\';';
    	else if ($ind==2) 
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree>60 and duree<=120 and titre_original=\''.$or.'\' and  titre_francais=\''.$fr.'\';';
		else if ($ind==3)
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree>120 and duree<=180 and titre_original=\''.$or.'\' and  titre_francais=\''.$fr.'\';';
		else
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree>180 and titre_original=\''.$or.'\' and  titre_francais=\''.$fr.'\';';
		return $this->getPdo()->query($q);		
    }



     public function ori_pay($or, $pa){
        $q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND titre_original=\''.$or.'\' and  pays=\''.$pa.'\';';
        return $this->getPdo()->query($q);
    }



    public function ori_pay_duree($or, $pa, $ind){
    	if($ind==1) 
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree<=60 and titre_original=\''.$or.'\' and  pays=\''.$pa.'\' ;';
    	else if ($ind==2) 
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree>60 and duree<=120 and titre_original=\''.$or.'\' and  pays=\''.$pa.'\';';
		else if ($ind==3)
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree>120 and duree<=180 and titre_original=\''.$or.'\' and   pays=\''.$pa.'\';';
		else
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree>180 and titre_original=\''.$or.'\' and  pays=\''.$pa.'\';';
		return $this->getPdo()->query($q);		
    }


    public function ori_duree($or, $ind){
    	if($ind==1) 
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree<=60 and titre_original=\''.$or.'\';';
    	else if ($ind==2) 
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree>60 and duree<=120 and titre_original=\''.$or.'\';;';
		else if ($ind==3)
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree>120 and duree<=180 and titre_original=\''.$or.'\';;';
		else
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree>180 and titre_original=\''.$or.'\';;';
		return $this->getPdo()->query($q);		
    }



	public function fr($fr){
        $q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND titre_francais=\''.$fr.'\';';
        return $this->getPdo()->query($q);
    }

    public function fr_pay($fr, $pa){
        $q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND titre_francais=\''.$fr.'\' and  pays=\''.$pa.'\';';
        return $this->getPdo()->query($q);
    }


    public function fr_duree($or, $ind){
    	if($ind==1) 
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree<=60 and titre_francais=\''.$or.'\';';
    	else if ($ind==2) 
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree>60 and duree<=120 and titre_francais=\''.$or.'\';;';
		else if ($ind==3)
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree>120 and duree<=180 and titre_francais=\''.$or.'\';;';
		else
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree>180 and titre_francais=\''.$or.'\';;';
		return $this->getPdo()->query($q);		
    }

    public function fr_pay_duree($fr, $pa, $ind){
    	if($ind==1) 
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree<=60 and titre_francais=\''.$fr.'\' and  pays=\''.$pa.'\';';
    	else if ($ind==2) 
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree>60 and duree<=120 and titre_francais=\''.$fr.'\' and  pays=\''.$pa.'\';';
		else if ($ind==3)
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree>120 and duree<=180 and titre_francais=\''.$fr.'\' and  pays=\''.$pa.'\';';
		else
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree>180 and titre_francais=\''.$fr.'\' and  pays=\''.$pa.'\';';
		return $this->getPdo()->query($q);		
    }


    public function pay($pa){
        $q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND pays=\''.$pa.'\';';
        return $this->getPdo()->query($q);
    }
 

    public function pay_duree( $pa, $ind){
    	if($ind==1) 
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree<=60 and  pays=\''.$pa.'\';';
    	else if ($ind==2) 
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree>60 and duree<=120 and  pays=\''.$pa.'\';';
		else if ($ind==3)
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree>120 and duree<=180 and  pays=\''.$pa.'\';';
		else
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree>180 and  pays=\''.$pa.'\';';
		return $this->getPdo()->query($q);		
    }


    public function duree($ind){
    	if($ind==1) 
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree<=60;';
    	else if ($ind==2) 
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree>60 and duree<=120;';
		else if ($ind==3)
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree>120 and duree<=180;';
		else
    			$q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree>180;';
		return $this->getPdo()->query($q);		
    }


    public function all($or, $fr, $pa, $ind){
        if($ind==1) 
                $q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree<=60 and  titre_original=\''.$or.'\' and titre_francais=\''.$fr.'\' and  pays=\''.$pa.'\';';
        else if ($ind==2) 
                $q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree>60 and duree<=120 and titre_original=\''.$or.'\' and titre_francais=\''.$fr.'\' and  pays=\''.$pa.'\';';
        else if ($ind==3)
                $q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree>120 and duree<=180 and titre_original=\''.$or.'\' and titre_francais=\''.$fr.'\' and  pays=\''.$pa.'\';';
        else
                $q = 'SELECT  * from films, individus where films.realisateur=individus.code_indiv AND duree>180;and titre_original=\''.$or.'\' and titre_francais=\''.$fr.'\' and  pays=\''.$pa.'\'';
        return $this->getPdo()->query($q);      
    }


}


?>