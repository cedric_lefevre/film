<?php
require_once 'base.php';

/**
* 
*/
class Genre extends base
{
	/*
	 * liste des films de la base de données
	 */ 
	public function liste_simple()
	{
		$q = 'SELECT * FROM genres order by code_genre';
		return $this->getPdo()->query($q);
	}
	
	
	/**
	 * recupérer les films du genre ayant $id
	*/ 
	public function get_films_genre($id){
		$q = '	SELECT 	*   from 	classification, genres, films 
				where	classification.ref_code_genre=genres.code_genre
				AND 	classification.ref_code_film=films.code_film
				AND 	code_genre = \''.$id.'\'
				order by code_film';
		return $this->getPdo()->query($q);
	}
	
	
	/**
	 * recupérer les genres du film ayant $id
	 */ 
	public function get_genre($id)
	{
		$q = 'SELECT * FROM genres where code_genre = \''.$id.'\'';
		return $this->getPdo()->query($q);
	}
}


?>
