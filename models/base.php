<?php

/**
* 
*/
class base
{
	
	private $pdo;
	/**
	 * fonction se connectant par PDO à la base de données
	 */ 
	public function __construct()
	{
		try {
		    $this->pdo = new PDO('mysql:dbname=DBamasson; host=info2', 'amasson', 'amasson', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
		} catch (PDOException $e) {
		    die('Connexion échouée : ' . $e->getMessage());
		}
	}
	
	/*
	 *  fonction récupérant la connection à la base de données
	 */ 
	public function getPdo()
	{
		return $this->pdo;
	}
}

?>
