<?php
require_once 'base.php';

/**
* 
*/
class Indiv extends base
{
	/**
	 * avoir la liste des nationnalitées des individus de la base
	 */ 

    public function pays(){
        $q = 'SELECT  distinct nationalite from  individus;';
        return $this->getPdo()->query($q);
    }
    
	/**
	 * individus ayant $id
	 */ 
	public function get_acteur($id){
		$q = 'SELECT * from individus WHERE code_indiv = \''.$id.'\';';
		return $this->getPdo()->query($q);
    }
    
	/**
	 * réalisateur ayant $id
	 */ 

	public function get_realisateur($id){
		$q = 'SELECT * FROM individus, films WHERE individus.code_indiv = films.code_film AND code_indiv = ( SELECT realisateur FROM films WHERE code_film =\''.$id.'\');';
		return $this->getPdo()->query($q);
    }

/*****	Fonctions pour la recherche d'individus   ******/

    public function get_act_name($name){
    	$q = 'SELECT 	* from 	individus where name=\''.$name.'\';';
    	return $this->getPdo()->query($q);
    }

    public function get_act_surname($sur){
        $q = 'SELECT    * from  individus where prenom=\''.$sur.'\';';
        return $this->getPdo()->query($q);
    }

    public function get_act_nat($nat){
        $q = 'SELECT    * from  individus where nationalite=\''.$nat.'\';';
        return $this->getPdo()->query($q);
    }

    public function get_act_name_surname($name, $sur){
        $q = 'SELECT    * from  individus where name=\''.$name.'\' and prenom=\''.$sur.'\';';
        return $this->getPdo()->query($q);
    }

    public function get_act_name_nat($name, $nat){
        $q = 'SELECT    * from  individus where name=\''.$name.'\' and nationalite=\''.$nat.'\';';
        return $this->getPdo()->query($q);
    }


    public function get_act_surname_nat($sur, $nat){
        $q = 'SELECT    * from  individus where prenom=\''.$sur.'\' and nationalite=\''.$nat.'\';';
        return $this->getPdo()->query($q);
    }

public function get_act_all($name, $sur, $nat){
        $q = 'SELECT    * from  individus where name=\''.$name.'\' and prenom=\''.$sur.'\' and nationalite=\''.$nat.'\';';
        return $this->getPdo()->query($q);
    }

}

/*******************************************************/
?>

